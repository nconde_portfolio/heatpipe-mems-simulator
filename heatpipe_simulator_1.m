syms dX1 dX2 dX3 dX4 dX5 r Uv Ul Pv Pl qx t real

sigma = .0644;              % Surface tension of water - air at 70 C [N/m]
alpha = 35*pi/180;          % Contact angle water - copper at 70 C [rad]
betal = 3*(sqrt(3)*sin(pi/3-alpha)^2 + 0.5*sin(2*(pi/3-alpha)) - (pi/3-alpha)); % Geometric area coefficient l [None]
betalw = 12*sin(pi/3-alpha); % Geometric area coefficient l [None]
betai = 6*(pi/3-alpha);     % Geometric area coefficient l [None]
d = 100e-6;                 % Width of one side of the equilateral pipe cross section [m]
w = 200e-6;                 % Width of pipe + space between pipes [m]
rhol = 977.77;              % Densidad del líquido [kg/m3]
rhov = .2;                  % Densidad del vapor [kg/m3]
hfg = 2.333e6;              % Latent heat of vaporization [J/kg]
k = 14.7;                   % duct Geometric constant [None]
Le = 280e-6;                % Length of the heat pipe evaporator [m]
La = 20e-3;                 % Length of the adiabatic part of the heat pipe [m]
Lc = 2.5e-3;                % Length of the condenser [m] (between 2 or 3, not specified)
L = Le+La+Lc;               % Total length [m]
mu = 403.5e-6;              % Dynamic viscosity of water
A = sqrt(3)/4*d;            % Area of cross section of the heat-pipe channel [m2]
rmax= d/(4*sin(pi/3-alpha)); % maximum radius of curvature
Ae = Le*d*3;                % Evaporator Area [m3]
l1 = Le/L;
l2 = (La+Le)/L;

eq1 = dX4 - dX5 + sigma*dX1/(r^2) == 0;

eq2 = r*dX3 + 2*Ul*dX1 - w/(rhol*betal*hfg)*qx/r == 0;

eq3 = (sqrt(3)/4*d^2 - betal*r^2)*dX2 - 2*betal*Uv*r*dX1 + w/(rhov*hfg)*qx == 0;

eq4 = -2*rhol*(r*Ul*dX3 + Ul^2*dX1) - r*dX5 + k*mu*betalw^2/(8*A*betal)*Ul*r + k*mu*betalw*betai/(8*A*betal)*Uv*r == 0;

eq5 = rhov*(sqrt(3)/2*d^2 - 2*betal*r^2)*Uv*dX2 - 2*rhov*betal*Uv^2*r*dX1 + (sqrt(3)/4*d^2 - betal*r^2)*dX4 + k*mu*betalw/(8*A)*Uv*r*(3*d - betalw*r + betai*r) == 0;

[eqA,eqB] = equationsToMatrix([eq1 eq2 eq3 eq4 eq5],[dX1 dX2 dX3 dX4 dX5]);

dX = simplify(inv(eqA)*eqB);

function dX = EqSys2(t,X,Q)
    d = 100e-6;                 % Width of one side of the equilateral pipe cross section [m]
    Le = 280e-6;                % Length of the heat pipe evaporator [m]
    La = 20e-3;                 % Length of the adiabatic part of the heat pipe [m]
    Lc = 2.5e-3;                % Length of the condenser [m] (between 2 or 3, not specified)
    L = Le+La+Lc;               % Total length [m]
    A = sqrt(3)/4*d;            % Area of cross section of the heat-pipe channel [m2]
    Ae = Le*d*3;                % Evaporator Area [m3]
    l1 = Le/L;
    l2 = (La+Le)/L;
    qe = Q/Ae;                  % Heat flux evaporator [W/m2]
    qa = 0;                     % Heat flux adiabatic [W/m2]
    qc = -qe/Le*La;              % Heat flux condenser [W/m2]
    
    if t<=l1
        qx = qe;
    elseif t>=l2
        qx = qc;
    else
        qx = 0;
    end
    
    disp([t,t/L])
    
    r  = X(1);
    Uv = X(2);
    Ul = X(3);
    Pv = X(4);
    Pl = X(5);

    dX = [-(601137806623860745872539319399126141923386982400000*Ul*r^2 - 234039969708638620965716129765099912175*Ul*qx - 158175700431495775077483676997889476652935777171275776000000*Ul*r^4 + 310322427283108306714205037142364604852495974400000*Uv*r^2 + 9356889819431517461603806352982186598933704076651659264*Uv*r^3 - 239829967979204609327932042971055247968508365717414871040000*Uv*r^4 + 25991360609531992948899462091617184997038066879587942400000*pi*Uv*r^4 + 61582279021078906363452927033390043257372672000*Ul*qx*r^2 - 61582279021078908396477746630975144306147328000*Uv*qx*r^2)/(118842243771396506390315925504*(- 6733994005691033031133888512000*Ul^2*r^3 + 25592163495128665212550*Ul^2*r + 1377418821541064469381120000*Uv^2*r^3 + 221764430268111379570360320*r^2 - 842803179217140043))
    (4239615991004255876874993883100898176285622078276581908807680*qx*r^2 - 16112420876324887921746916242858590695757966875820032*qx + 265392134640404157653812864386070385700422332963535399682048000000*Uv^2*r^3 + 8002144687108040040614422063682020087308460574773434332345231348858880*Uv^2*r^4 - 205105985120030268942692068332133637857887948902939318716831898586316800000*Uv^2*r^5 + 22228179686411222335040061288005611353634612707703984256514531524608000000*pi*Uv^2*r^5 - 128738177873423331327082694691911963038099152157655077421580288000*Ul^2*qx*r^3 - 26333018577666185570652135919881355132208832784326595706880000*Uv^2*qx*r^3 + 514101565619072918726630051718926674896008111584526003601408000000*Ul*Uv*r^3 - 135274099114526903378878413006459394946790602780457353437956192337920000000*Ul*Uv*r^5 + 489262166467210734883431127167075907596002089197843251200*Ul^2*qx*r + 52666037155332369402632654149387722699446835865429726986240000*Ul*Uv*qx*r^3 - 200154296600309160122052174249534025883820315055541625*Ul*Uv*qx*r)/(36893488147419103232*(1377418821541064469381120*r^2 - 5234802355385963)*(- 6733994005691033031133888512000*Ul^2*r^3 + 25592163495128665212550*Ul^2*r + 1377418821541064469381120000*Uv^2*r^3 + 221764430268111379570360320*r^2 - 842803179217140043));
    (4056065096042506489058130747466499059875840*qx*r^2 - 15414846077540042331215028848167191*qx + 2404551226495442983490157277596504567693547929600000*Ul^2*r^3 - 632702801725983100309934707991557906611743108685103104000000*Ul^2*r^5 + 123164558042157812726905854066780086514745344000*Ul^2*qx*r^3 + 25192950907096313596634352468736019005440000*Uv^2*qx*r^3 + 1241289709132433226856820148569458419409983897600000*Ul*Uv*r^3 + 37427559277726069846415225411928746395734816306606637056*Ul*Uv*r^4 - 959319871916818437311728171884220991874033462869659484160000*Ul*Uv*r^5 - 468079939417277241931432259530199824350*Ul^2*qx*r + 103965442438127971795597848366468739988152267518351769600000*pi*Ul*Uv*r^5 - 246329116084315633585910986523900577224589312000*Ul*Uv*qx*r^3)/(237684487542793012780631851008*r^2*(- 6733994005691033031133888512000*Ul^2*r^3 + 25592163495128665212550*Ul^2*r + 1377418821541064469381120000*Uv^2*r^3 + 221764430268111379570360320*r^2 - 842803179217140043));
    
end

function dX = EqSys3(t,X,Q)
    d = 100e-6;                 % Width of one side of the equilateral pipe cross section [m]
    Le = 280e-6;                % Length of the heat pipe evaporator [m]
    La = 20e-3;                 % Length of the adiabatic part of the heat pipe [m]
    Lc = 2.5e-3;                % Length of the condenser [m] (between 2 or 3, not specified)
    L = Le+La+Lc;               % Total length [m]
    A = sqrt(3)/4*d;            % Area of cross section of the heat-pipe channel [m2]
    Ae = Le*d*3;                % Evaporator Area [m3]
    l1 = Le/L;
    l2 = (La+Le)/L;
    qe = Q/Ae;                  % Heat flux evaporator [W/m2]
    qa = 0;                     % Heat flux adiabatic [W/m2]
    qc = -qe/Le*La;              % Heat flux condenser [W/m2]
    
    if t<=l1
        qx = qe;
    elseif t>=l2
        qx = qc;
    else
        qx = 0;
    end
    
    disp([t,t/L])
    
    r  = X(1);
    Uv = X(2);
    Ul = X(3);
    Pv = X(4);
    Pl = X(5);
    
    eqA = [0.0644/r^2,           0,          0,          1,          -1;
          2*Ul,                 0,          r,          0,          0;
          -2.278748289088814*Uv*r,  4.330127018922193e-09-1.139374144544407*r^2,    0,  0,  0;
          -1955.54*Ul^2,        0,  -1955.54*Ul*r,      0,          -r;
          -0.455749657817763*Uv^2*r,    -Uv*(0.455749657817763*r^2-1.732050807568877e-09),  0,  4.330127018922193e-09-1.139374144544407*r^2,    0];
    
    eqB = [0;
          7.695063160925169e-14*qx/r;
          -4.286326618088299e-10*qx;
          -(386.5119286208230*Ul - 199.5271608969676*Uv)*r;
          -86.835989255546863*Uv*r*(7.689413018879888*r + 3e-4)];
    
    dX = inv(eqA)*eqB;
    
end

function Y = ode4(odefun,tspan,y0,varargin)
%ODE4  Solve differential equations with a non-adaptive method of order 4.
%   Y = ODE4(ODEFUN,TSPAN,Y0) with TSPAN = [T1, T2, T3, ... TN] integrates 
%   the system of differential equations y' = f(t,y) by stepping from T0 to 
%   T1 to TN. Function ODEFUN(T,Y) must return f(t,y) in a column vector.
%   The vector Y0 is the initial conditions at T0. Each row in the solution 
%   array Y corresponds to a time specified in TSPAN.
%
%   Y = ODE4(ODEFUN,TSPAN,Y0,P1,P2...) passes the additional parameters 
%   P1,P2... to the derivative function as ODEFUN(T,Y,P1,P2...). 
%
%   This is a non-adaptive solver. The step sequence is determined by TSPAN
%   but the derivative function ODEFUN is evaluated multiple times per step.
%   The solver implements the classical Runge-Kutta method of order 4.   
%
%   Example 
%         tspan = 0:0.1:20;
%         y = ode4(@vdp1,tspan,[2 0]);  
%         plot(tspan,y(:,1));
%     solves the system y' = vdp1(t,y) with a constant step size of 0.1, 
%     and plots the first component of the solution.   
%

%  if ~isnumeric(tspan)
%    error('TSPAN should be a vector of integration steps.');
%  end
%  
%  if ~isnumeric(y0)
%    error('Y0 should be a vector of initial conditions.');
%  end

h = diff(tspan);
%  if any(sign(h(1))*h <= 0)
%    error('Entries of TSPAN are not in order.') 
%  end  

%  try
%    f0 = feval(odefun,tspan(1),y0,varargin{:});
%  catch
%    msg = ['Unable to evaluate the ODEFUN at t0,y0. ',lasterr];
%    error(msg);  
%  end  

y0 = y0(:);   % Make a column vector.
%  if ~isequal(size(y0),size(f0))
%    error('Inconsistent sizes of Y0 and f(t0,y0).');
%  end  

neq = length(y0);
N = length(tspan);
Y = zeros(neq,N);
F = zeros(neq,4);

Y(:,1) = y0;
for i = 2:N
    ti = tspan(i-1);
    hi = h(i-1);
    yi = Y(:,i-1);
    F(:,1) = feval(odefun,ti,yi,varargin{:});
    F(:,2) = feval(odefun,ti+0.5*hi,yi+0.5*hi*F(:,1),varargin{:});
    F(:,3) = feval(odefun,ti+0.5*hi,yi+0.5*hi*F(:,2),varargin{:});  
    F(:,4) = feval(odefun,tspan(i),yi+hi*F(:,3),varargin{:});
Y(:,i) = yi + (hi/6)*(F(:,1) + 2*F(:,2) + 2*F(:,3) + F(:,4));
end
Y = Y.';

end

xspan = xspan(1:100:end);
X = X(1:100:end,:);

xn = xspan/L;
clear xspan
Dh = 4*A./(12*X(:,1)*sin(pi/3-alpha));
clearvars -except X Dh alpha A hfg sigma rhov rhol xn
rnorm = X(:,1).^2*3*sin(pi/3-alpha)/A;
X = X(:,2:5);
Pvnorm = X(:,3).*Dh/sigma;
X = X(:,[1,2,4]);
Plnorm = X(:,3).*Dh/sigma;
X = X(:,1:2);
Dh = Dh.^2;
Uvnorm = X(:,1)*rhov.*Dh.^2*hfg;
X = X(:,2);
Ulnorm = X(:,1)*rhol.*Dh.^2*hfg;
clear X Dh

clearvars -except rnorm Uvnorm Ulnorm Pvnorm Plnorm xn

figure(1)
plot(xn,rnorm)    % /Dh, Dh = 4A/Pwet, Pwet = 12*r*sin(pi/3-al) 
grid on
title('r vs x')
xlabel('x* = x/L')
ylabel('r* = r/Dh = 3sin(pi/3-alpha)r^2/A')
figure(2)
hold on
plot(xn,Uvnorm)
plot(xn,Ulnorm)
grid on
title('U vs x')
xlabel('x* = x/L')
ylabel('U* = U/Uref = U*rho*Dh^2*Hfg/Q')
legend('Uv','Ul')
figure(3)
hold on
plot(xn,Pvnorm)
plot(xn,Plnorm)
grid on
title('P vs x')
xlabel('x* = x/L')
ylabel('P* = P/Pref = P*Dh/sigma')
legend('Pv','Pl')