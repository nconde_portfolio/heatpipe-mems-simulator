%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constants and Variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% faltan Kp (K') y m
syms Kp m real

%  Fluid propoerties
% cold 32 C, hot = cold + 5 C, properties at 34.5 C
sigma = 14.36e-3;
rho1 = 611.3;
lambda = 26.4e3;
gamma = 0;
mu = 0.2224e-3;


%  Geometric properties
L = 20e-3;
Lc = L/3;
La = L/3;
Le = L/3;
f1 = Lc/L;                      % Nondimensional coordinate of the junction of condensing and adiabatic sections
f2 = (Lc+La)/L;                 % Nondimensional coordinate of the junction of adiabatic and evaporative sections
a = 400e-6;                     % Side length of the polygon [m]
Wb = 3*a;                       % Perimeter of the heat pipe polygon [m]
alpha = pi/6;                   % Half apex angle of polygon [rad], triangle
alga = alpha+gamma;
R0 = a*sin(alpha)/(2*cos(alga));% Reference radius [rad]
%  R0 = 115.4e-6    % YES
phi = pi-2*(alga);              % Curvature [rad]
B1 = 3*(cot(alga)-phi/2+cot(alga)*cos(alga)*sin(gamma)/sin(alpha));
%  B1*R0^3 = 3.161E-12  % YES
B2 = mu*Kp*cos(alga)^2/(2*sin(alpha)^2*(B1/3)^2);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Critical heat flux
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% m
Qcr = sigma*rho1*lambda*B1*R0^3/(3*B2*L*(f2-f1)+3*B2*L*(1-f2+f1)/L^m*(m+1)/(m+2));
Qcrv = 0.03102;

% Kp
B2v = 0.008478;
K1 = double(solve(B2 == B2v, Kp))

B2p = B2*L/(sigma*R0^3*rho1*lambda*B1);
B2pv = 16.1596;
K2 = double(solve(B2p == B2pv, Kp))

gp = B1*R0^3/B2;
gpv = 3.7284E-10;
K3 = double(solve(gp == gpv, Kp))

B = 1/(B2p*0.015);
Bv = 4.1254;
K4 = double(solve(B == Bv, Kp))

B2v*L/(sigma*R0^3*rho1*lambda*B1)

B1*R0^3/B2v

1/(B2pv*0.015)

1/(231.3043*0.015)

mu*K1*cos(alga)^2/(2*sin(alpha)^2*(B1/3)^2)
mu*K2*cos(alga)^2/(2*sin(alpha)^2*(B1/3)^2)

Q = subs(Qcr,Kp,K)
double(solve(Q==Qcrv,m))

mv = 0:0.01:2;
Q = simplify(subs(Qcr,Kp,K))
plot(mv,double(subs(Q,m,mv)))
ylabel('Q_{cr} [W]')
xlabel('m')
grid on
title('Q_{cr} vs m')